using System;

namespace sharpsvr.attributes
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class SharpSerializerAttribute : Attribute
    {
        public string Name { get; set; }
    }
}